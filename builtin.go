package currency

import (
	"math"
)

// NewFromSubunitsINR returns a GenericCurrency representing INR
// with the amount initialized with the give amount in subunits
// i.e. Paise.
func NewFromSubunitsINR(amountInSubunits int) *GenericCurrency {
	return &GenericCurrency{
		SubunitFactor:    100,
		StringPrefix:     "Rs. ",
		AmountInSubunits: amountInSubunits,
	}
}

// NewFromAmountINR returns a new GenericCurrency representing
// an INR with its amount initialized with the given amount in
// Rupees.
func NewFromAmountINR(amount float64) *GenericCurrency {
	amountInSubunits := int(math.Round(amount * 100))
	return NewFromSubunitsINR(amountInSubunits)
}

// NewINR returns a new GenericCurrency representing an INR.
func NewINR() *GenericCurrency {
	return NewFromSubunitsINR(0)
}
