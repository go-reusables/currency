package currency

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func helper(factor uint, amount int, prefix, suffix string) *GenericCurrency {
	return &GenericCurrency{
		SubunitFactor:    factor,
		StringPrefix:     prefix,
		StringSuffix:     suffix,
		AmountInSubunits: amount,
	}
}

func TestGetSubunitFactor(t *testing.T) {
	dollar := helper(100, 0, "USD", "")
	assert.Equal(t, dollar.GetSubunitFactor(), uint(100))

	sample := helper(0, 0, "XXX", "")
	assert.Equal(t, sample.GetSubunitFactor(), uint(0))
}

func TestGetAmountInSubunits(t *testing.T) {
	dollar := helper(100, 100, "USD", "")
	assert.Equal(t, dollar.GetAmountInSubunits(), 100)

	dollar = helper(100, 0, "USD", "")
	assert.Equal(t, dollar.GetAmountInSubunits(), 0)

	dollar = helper(100, -100, "USD", "")
	assert.Equal(t, dollar.GetAmountInSubunits(), -100)
}

func TestCopy(t *testing.T) {
	dollar := helper(100, 100, "USD", "")
	inr := dollar.Copy(dollar.GetAmountInSubunits())

	assert.Equal(t, dollar.GetAmountInSubunits(), inr.GetAmountInSubunits())
	assert.Equal(t, dollar.GetSubunitFactor(), inr.GetSubunitFactor())
	assert.Equal(t, dollar.StringPrefix, inr.StringPrefix)
	assert.Equal(t, dollar.StringSuffix, inr.StringSuffix)

	t.Log(fmt.Sprintln("Updates might be necessary"))
}

func TestString(t *testing.T) {
	inr := helper(100, 0, "Rs. ", "")
	assert.Equal(t, inr.String(), "Rs. 0.00")

	inr.AmountInSubunits = 620
	assert.Equal(t, inr.String(), "Rs. 6.20")

	inr.AmountInSubunits = 1
	assert.Equal(t, inr.String(), "Rs. 0.01")

	inr.AmountInSubunits = 110000
	assert.Equal(t, inr.String(), "Rs. 1100.00")

	inr.AmountInSubunits = -120
	assert.Equal(t, inr.String(), "Rs. -1.20")
}

func TestAdd(t *testing.T) {
	inr := helper(100, 0, "", "")
	inrSum := inr.Add(inr.Copy(100))
	assert.Equal(t, inrSum.GetAmountInSubunits(), 100)

	inr = helper(100, -102, "", "")
	inrSum = inr.Add(inr.Copy(100))
	assert.Equal(t, inrSum.GetAmountInSubunits(), -2)
}

func TestSubtract(t *testing.T) {
	inr := helper(100, 0, "", "")
	inrSum := inr.Subtract(inr.Copy(100))
	assert.Equal(t, inrSum.GetAmountInSubunits(), -100)

	inr = helper(100, 102, "", "")
	inrSum = inr.Subtract(inr.Copy(100))
	assert.Equal(t, inrSum.GetAmountInSubunits(), 2)

	inr = helper(100, -100, "", "")
	inrSum = inr.Subtract(inr.Copy(100))
	assert.Equal(t, inrSum.GetAmountInSubunits(), -200)
}
