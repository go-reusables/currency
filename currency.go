package currency

// Currency represents typical requirements for a data structure
// to be a currency implementation. This currency is meant to handle
// currencies in their lowest denomination to avoid accumulation of
// error and calculate floating representations on demand during string
// representations. By default the rounding happens on the second
// decimal place.
type Currency interface {
	String() string
	GetSubunitFactor() uint
	GetAmountInSubunits() int
}
