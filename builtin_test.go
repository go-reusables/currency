package currency

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	inrSubunitFactor uint = 100
)

func testGeneral(t *testing.T, currency *GenericCurrency, rep string) {
	assert.Equal(t, currency.GetSubunitFactor(), inrSubunitFactor)
	assert.Equal(t, currency.String(), rep)
}

func testInternal(t *testing.T, currency *GenericCurrency, amountInSubunits int) {
	assert.Equal(t, currency.GetAmountInSubunits(), amountInSubunits)
}

func TestNewINR(t *testing.T) {
	newINR := NewINR()
	testGeneral(t, newINR, "Rs. 0.00")
	testInternal(t, newINR, 0)
}

func TestNewFromAmountINR(t *testing.T) {
	newINR := NewFromAmountINR(0)
	testGeneral(t, newINR, "Rs. 0.00")
	testInternal(t, newINR, 0)

	newINR = NewFromAmountINR(0.50)
	testInternal(t, newINR, 50)

	newINR = NewFromAmountINR(10.33)
	testInternal(t, newINR, 1033)

	// Test lower bound rounding capability
	newINR = NewFromAmountINR(0.033)
	testInternal(t, newINR, 3)

	// Test upper bound rounding capability
	newINR = NewFromAmountINR(0.036)
	testInternal(t, newINR, 4)

	newINR = NewFromAmountINR(-0.036)
	testInternal(t, newINR, -4)
}

func TestNewFromSubunitsINR(t *testing.T) {
	newINR := NewFromSubunitsINR(0)
	testGeneral(t, newINR, "Rs. 0.00")
	testInternal(t, newINR, 0)

	newINR = NewFromSubunitsINR(50)
	testInternal(t, newINR, 50)

	newINR = NewFromSubunitsINR(-60)
	testInternal(t, newINR, -60)
}
