package currency

import (
	"fmt"
	"math"
	"strings"
)

// GenericCurrency represents a modern generic currency i.e.
// a currency without complex subunit to unit conversion means.
// It facilitates proper string representation as well.
// It implements the Currency interface.
type GenericCurrency struct {
	SubunitFactor    uint
	StringPrefix     string
	StringSuffix     string
	AmountInSubunits int
}

// GetSubunitFactor returns the subunit factor for a currency.
func (generic GenericCurrency) GetSubunitFactor() uint {
	return generic.SubunitFactor
}

// GetAmountInSubunits irrespective of the internal representation
// returns the current amount in the multiples of subunits.
func (generic GenericCurrency) GetAmountInSubunits() int {
	return generic.AmountInSubunits
}

// Copy returns a copy of the currency with an updated amount value.
func (generic GenericCurrency) Copy(amountInSubunits int) GenericCurrency {
	// Assuming GenericCurrency struct is not deep
	newGenericCurrency := generic
	newGenericCurrency.AmountInSubunits = amountInSubunits

	return newGenericCurrency
}

// Add returns the sum of two currencies. Calculations are always done
// in terms of the first currency. The second currency is not checked to
// be same but is assumed to be as such.
func (generic GenericCurrency) Add(with Currency) GenericCurrency {
	totalAmount := generic.GetAmountInSubunits() + with.GetAmountInSubunits()
	return generic.Copy(totalAmount)
}

// Subtract returns the difference between two currencies. Like Add,
// Subtract also work in terms of the first term or receiver instance.
// The second currency is not checked to be as the first same currency.
func (generic GenericCurrency) Subtract(by Currency) GenericCurrency {
	negatedAmount := -1 * by.GetAmountInSubunits()
	return generic.Add(generic.Copy(negatedAmount))
}

func (generic GenericCurrency) String() string {
	floatAmountInSubunits := float64(generic.GetAmountInSubunits())
	floatSubunitFactor := float64(generic.GetSubunitFactor())

	amountInFloat := floatAmountInSubunits / floatSubunitFactor
	amountInFloat = math.Round(amountInFloat*100) / 100

	parts := []string{
		generic.StringPrefix,
		fmt.Sprintf("%.2f", amountInFloat),
		generic.StringSuffix,
	}

	return strings.Join(parts[:], "")
}
